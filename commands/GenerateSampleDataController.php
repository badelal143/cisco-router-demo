<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GenerateSampleDataController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($n = 0)
    {
        
        if($n <= 0) {
            echo "Please pass option n, e.g. --n 10 to generate/insert randon 10 records in the table.";
            return ExitCode::OK;
        } else {
            $connection = \Yii::$app->db;
            for($i=0; $i<$n; $i++) {
                $rand = rand(999,999);
                $hostname = rand(1, 99).'.'.rand(1, 99).'.'.rand(1, 99).'.'.rand(1, 99);
                $data = [
                    'sap_id' => 'S'.time().$rand,
                    'hostname' => $hostname,
                    'loopback' => 'L'.time().$rand,
                    'mac_address' => 'MAC::'.$rand.rand(1,99),
                    'created_at' => date('Y-m-d H:i:s'),
                ];

                $res = $connection->createCommand()->insert('routers', $data)->execute();
                echo ($i+1)."/$n record inserted \n";
            }
        }

        return ExitCode::OK;
    }
}
