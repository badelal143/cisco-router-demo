<?php

namespace app\controllers;

use Yii;
use app\models\Routers;
use app\models\RoutersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RoutersController implements the CRUD actions for Routers model.
 */
class RoutersController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Routers models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // $this->enableCsrfValidation = false;
        $searchModel = new RoutersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Routers();


        if ($id) {
            $model = $this->findModel($id);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single Routers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Routers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Routers();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('create', [
    //         'model' => $model,
    //     ]);
    // }

    public function actionAjaxCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Routers();

        if (!Yii::$app->request->isPost) {
            $res = [
                'status'=>'failed',
                'message'=>'Only post methond is allowed',
            ];
            return $this->asJson($res);
        }
        
        $model->load(Yii::$app->request->post());
        
        if ($model->validate()) {
            $model->save(false);
            $res = [
                'status'=>'success',
                'message'=>'Record created successfully',
            ];
            
            return $this->asJson($res);
        } else {
            $res = [
                'status'=>'failed',
                'message'=>'Validation failed.',
                'errors'=>$model->getErrors()
            ];
            return $this->asJson($res);
        }
    }


    /**
     * Updates an existing Routers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);
        
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('update', [
    //         'model' => $model,
    //     ]);
    // }

    public function actionAjaxUpdate($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!Yii::$app->request->isPost) {
            $res = [
                'status'=>'failed',
                'message'=>'Only post methond is allowed',
            ];
            return $this->asJson($res);
        }

        $model = $this->findModelByID($id);
        
        if(!$model) {
            $res = [
                'status'=>'failed',
                'message'=>'Recored not found.',
            ];
            return $this->asJson($res);
        }
        
        $model->load(Yii::$app->request->post());
        
        if ($model->validate()) {
            $model->save(false);
            $res = [
                'status'=>'success',
                'message'=>'Record updated successfully',
            ];
            
            return $this->asJson($res);
        } else {
            $res = [
                'status'=>'failed',
                'message'=>'Validation failed.',
                'errors'=>$model->getErrors()
            ];
            return $this->asJson($res);
        }
        
        
    }

    /**
     * Deletes an existing Routers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     //$this->findModel($id)->delete();
    //     $model = $this->findModel($id);
    //     $model->deleted = 1;
    //     $model->deleted_by = 1; // Put here user id of user from session
    //     $model->deleted_at = date('Y-m-d H:i:s');
    //     $model->save();
    //     return $this->redirect(['index']);
    // }


    public function actionAjaxDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        //$model->deleted = ($model->deleted === 1 )? 0 : 1;
        $model->deleted = 1;
        $model->deleted_by = 1; // Put here user id of user from session
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $res = [
            'status'=>'success',
            'message'=>'Record deleted successfully',
        ];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $this->asJson($res);
        // return $this->redirect(['index']);
    }



    /**
     * Finds the Routers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Routers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Routers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelByID($id)
    {   
        if (($model = Routers::findOne($id)) !== null) {
            return $model;
        }
        return null;
    }
}
