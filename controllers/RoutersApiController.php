<?php

namespace app\controllers;

use Yii;
use app\models\Routers;
use app\models\RoutersSearch;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use sizeg\jwt\Jwt;
use sizeg\jwt\JwtHttpBearerAuth;

/**
 * RoutersApiController implements the CRUD actions for Routers model.
 */
class RoutersApiController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'login',
            ],            
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
            ]
        ];
        return $behaviors;
    }

    public function actionLogin() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost) {
            $res = [
                'status'=>'failed',
                'message'=>'Only POST method is allowed',
            ];
            return $this->asJson($res);
        }

        $data = [
            'LoginForm' => Yii::$app->request->post()
        ];

        $model = new \app\models\LoginForm();
        $model->load($data);

        if ($model->validate() && $model->login()) {
            
            $jwt = Yii::$app->jwt;
            $signer = $jwt->getSigner('HS256');
            $key = $jwt->getKey();
            $time = time();
            $token = $jwt->getBuilder()
                ->issuedBy('http://example.com')// Configures the issuer (iss claim)
                ->permittedFor('http://example.org')// Configures the audience (aud claim)
                ->identifiedBy('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
                ->issuedAt($time)// Configures the time that the token was issue (iat claim)
                ->expiresAt($time + 3600)// Configures the expiration time of the token (exp claim)
                ->withClaim('uid', 100)// Configures a new claim, called "uid"
                ->getToken($signer, $key); // Retrieves the generated token

            $res = [
                'status'=>'success',
                'message'=>'Login in successfully',
                'token'=> (string)$token
            ];
            return $this->asJson($res);
        } else {
            $LoginErrors = $model->getErrors();
            $errors = [];
            if(isset($LoginErrors['username'][0])) {
                $errors['username'] = $LoginErrors['username'][0];
            }
            if(isset($LoginErrors['password'][0])) {
                $errors['password'] = $LoginErrors['password'][0];
            }
            $res = [
                'status'=>'failed',
                'message'=>'Invalid username OR password',
                'errors'=> $errors
            ];
            return $this->asJson($res);
        }


        
    }

    /**
     * Lists all Routers models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $this->enableCsrfValidation = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $searchModel = new RoutersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $rows = $dataProvider->getModels();
        
        $res = [
            'status'=>'success',
            'message'=>'Routers',
            'total'=>count($rows),
            'rows'=>$rows,
        ];
        return $this->asJson($res);
    }


    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Routers();

        if (!Yii::$app->request->isPost) {
            $res = [
                'status'=>'failed',
                'message'=>'Only post methond is allowed',
            ];
            return $this->asJson($res);
        }
        
        $data = ['Routers'=> Yii::$app->request->post()];
        $model->load($data);
        
        if ($model->validate()) {
            $model->save(false);
            $res = [
                'status'=>'success',
                'message'=>'Record created successfully',
            ];
            
            return $this->asJson($res);
        } else {
            $res = [
                'status'=>'failed',
                'message'=>'Validation failed.',
                'errors'=>$model->getErrors()
            ];
            return $this->asJson($res);
        }
    }



    public function actionUpdate($hostname)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (!Yii::$app->request->isPost) {
            $res = [
                'status'=>'failed',
                'message'=>'Only POST methond is allowed',
            ];
            return $this->asJson($res);
        }

        $model = $this->findModelByIP($hostname);
        // $new_model = new Routers();
        
        if(!$model) {
            $res = [
                'status'=>'failed',
                'message'=>'Recored not found.',
            ];
            return $this->asJson($res);
        }
        
        
        $postdata = Yii::$app->request->post();
        // print_r($postdata);
        $data = [];
        $data['Routers']['sap_id'] = isset($postdata['sap_id']) ? $postdata['sap_id'] : '';
        //$data['Routers']['hostname'] = isset($postdata['hostname']) ? $postdata['hostname'] : '';
        $data['Routers']['loopback'] = isset($postdata['loopback']) ? $postdata['loopback'] : '';
        $data['Routers']['mac_address'] = isset($postdata['mac_address']) ? $postdata['mac_address'] : '';

        $model->load($data);

        if ($model->validate()) {
            $model->save(false);
            $res = [
                'status'=>'success',
                'message'=>'Record updated successfully',
            ];
            
            return $this->asJson($res);
        } else {
            $res = [
                'status'=>'failed',
                'message'=>'Validation failed.',
                'errors'=>$model->getErrors()
            ];
            return $this->asJson($res);
        }
        
        
    }

    public function actionDelete($hostname)
    {
        //$this->findModel($id)->delete();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = $this->findModelByIP($hostname);
        
        if(!$model) {
            $res = [
                'status'=>'failed',
                'message'=>'Recored not found.',
            ];
            return $this->asJson($res);
        }

        $model->delete();
        $res = [
            'status'=>'success',
            'message'=>'Record deleted successfully',
        ];
        
        return $this->asJson($res);
    }

    protected function findModelByIP($hostname)
    {   
        if (($model = Routers::findOne(['hostname' => $hostname])) !== null) {
            return $model;
        }
        return null;
    }
}
