<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Routers */
/* @var $form yii\widgets\ActiveForm */
?>

<h3 class="widget-title">
    <?php if (!$model->isNewRecord) : ?>
    <i class="ace-icon glyphicon glyphicon-edit"></i> Update <?php echo Html::encode($this->title); ?>
    <?php else : ?>
    <i class="ace-icon glyphicon glyphicon-plus"></i> Add <?php echo Html::encode($this->title); ?>
    <?php endif; ?>
</h3>
<hr/>
<div class="routers-form">
    <?php
    $form_action = ($model->isNewRecord) ? ['routers/ajax-create'] : ['routers/ajax-update', 'id'=>$model->id];
    ?>
    <?php $form = ActiveForm::begin(['action'=>$form_action, 'id'=>'form-routers']); ?>
    
    <?php echo $form->field($model, 'sap_id')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'hostname')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'loopback')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'mac_address')->textInput(['maxlength' => true]); ?>


    <div class="form-group">
        <?php echo Html::submitButton('Save', ['id'=>'save-btn', 'class' => 'btn btn-success']); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script>
//  $(function(){
    // $( "form#form-routers" ).submit(function( event ) {
        // event.preventDefault();
        // const form = document.querySelector('form#form-routers');
        // const data = Object.fromEntries(new FormData(form).entries());
        // console.log(form);
        // console.log(data);
        // $.ajax({          
        //     type: 'POST',
        //     dataType: 'json',
        //     data: data,
        //     cache: false,
        //     url: '<?php echo Yii::$app->urlManager->createUrl(['routers/ajax-update', 'id' => $model->id]); ?>',
        //     success: function(response) {
        //     if(response.status == 'success') {
        //         alert(response.message);                                                
        //     }
        //         $.pjax.reload({container: '#" . $pjaxgridview_id . "'});
        //     }
        //     return false;
        // });
        // return false;
    // });
// });

// $(function() {
//     $(document).on("submit", "form#form-routers", function(event) {
//         event.preventDefault(); // stopping submitting
//         var data = $(this).serializeArray();
//         var url = $(this).attr('action');
//         $.ajax({
//             url: url,
//             type: 'post',
//             dataType: 'json',
//             data: data
//         })
//         .done(function(response) {
//             if (response.status == 'success') {
//                 alert("Wow you commented");
//                 $.pjax.reload({container: '#router'});
//             }
//         })
//         .fail(function() {
//             console.log("error");
//         });
    
//     });
// });
</script>
