<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Routers */

$this->title = 'Create Routers';
$this->params['breadcrumbs'][] = ['label' => 'Routers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
	<div class="col-md-4">
	  	<div class="routers-create">
		    <h1><?= Html::encode($this->title) ?></h1>
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
	<div class="col-md-8">
	  One of three columns
	</div>
</div>
