<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RoutersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Routers';
$this->params['breadcrumbs'][] = $this->title;
$pjaxgridview_id = 'router';
?>

<?php Pjax::begin(['id' => $pjaxgridview_id, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]); ?>
    
<div class="row">
    <div class="col-md-3">
        <?php //Pjax::begin(['id' => $pjaxgridview_id.'form', 'enablePushState' => false]); ?> 
        <?php echo $this->render('_form', ['model' => $model]); ?>
        <?php //Pjax::end(); ?>
    </div>
    <div class="col-md-9">    
        <div class="routers-index">
        <h1><?= Html::encode($this->title) ?> <?= Html::a('Create Routers', ['routers/index'], ['class' => 'btn btn-success']) ?></h1>
        
        <?php //Pjax::begin(); ?>
        <?php //Pjax::begin(['id' => $pjaxgridview_id, 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'id' => 'grid-view',
            'options' => ['class' => 'table-responsive'],
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\ActionColumn', 
                    'template' => '{update} {delete}',
                    'visibleButtons' => [
                        'delete' => function ($model, $key, $index) {
                            return $model->deleted !== 1;
                        }
                    ],
                    'buttons' => [
                        'update' => function ($url, $model) use ($pjaxgridview_id) {
                            $url = ['routers/index', 'id'  =>$model->id];
                            return Html::a(
                                "<span class='glyphicon glyphicon-edit'></span>",
                                $url,
                                ['data-pjax' => '1']
                            );
                        },
                        'delete' => function ($url, $model) use ($pjaxgridview_id) {
                            $url = ['routers/ajax-delete', 'id'  =>$model->id];
                            return Html::a("<span class='glyphicon glyphicon-trash'></span>", '#', [
                                'class' => 'arr-img pull-left',
                                'title' => 'Click here to delete',
                                'onclick' => "
                                if (confirm('Are you sure you want to delete this record ?') == true) 
                                    {                            
                                        $.ajax({          
                                            type: 'POST',
                                            dataType: 'json',
                                            cache: false,
                                            url: '" . Yii::$app->urlManager->createUrl(['routers/ajax-delete', 'id' => $model->id]) . "',
                                            success: function(response) {
                                                if(response.status == 'success') {
                                                    alert(response.message);
                                                    $.pjax.reload({container: '#" . $pjaxgridview_id . "'});                                                
                                                } else {
                                                    alert(response.errors);
                                                }
                                                
                                            }
                                        });
                                    };
                                    return false;
                                "
                            ]);
                        },
                    ],
                ],
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'sap_id',
                'hostname',
                'loopback',
                'mac_address',
                
                [
                    'filter' => false,
                    'attribute' => 'deleted',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {                        
                        return ($model->deleted == 1) ? 'YES' : 'NO';
                    }
                ],
                'created_at',
                'updated_at',
                'created_by',
                'updated_by',

                
            ],
        ]); ?>

        <?php //Pjax::end(); ?>

    </div>
    </div>
</div
<?php Pjax::end(); ?>>

<script>
$(function() {
    $(document).on("submit", "form#form-routers", function(event) {
        event.preventDefault(); // stopping submitting
        var data = $(this).serializeArray();
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: data
        })
        .done(function(response) {
            if (response.status == 'success') {
                alert(response.message);
                $.pjax.reload({container: '#router'});
            } else {
                alert(JSON.stringify(response.errors));
            }
        })
        .fail(function() {
            console.log("error");
        });
    
    });
});
</script>