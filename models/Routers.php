<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%routers}}".
 *
 * @property int $id
 * @property string $sap_id
 * @property string $hostname
 * @property string $loopback
 * @property string $mac_address
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Routers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%routers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sap_id', 'hostname', 'loopback', 'mac_address', /*'created_at', 'updated_at', 'created_by', 'updated_by'*/], 'required'],
            [['created_at', 'updated_at', 'deleted', 'deleted_at', 'deleted_by'], 'safe'],
            [['created_by', 'updated_by', 'deleted', 'deleted_by'], 'integer'],
            [['sap_id'], 'string', 'max' => 18],
            [['hostname'], 'string', 'max' => 14],
            [['hostname', 'loopback'], 'unique'],

            ['hostname', 'ip'], // IPv4 or IPv6 address
            // ['hostname', 'ip', 'ipv6' => false], // IPv4 address (IPv6 is disabled)
            // ['hostname', 'ip', 'subnet' => true], // requires a CIDR prefix (like 10.0.0.1/24) for the IP address
            // ['hostname', 'ip', 'subnet' => null], // CIDR prefix is optional
            // ['hostname', 'ip', 'subnet' => null, 'normalize' => true], // CIDR prefix is optional and will be added when missing
            // ['hostname', 'ip', 'ranges' => ['192.168.0.0/24']], // only IP addresses from the specified subnet are allowed
            // ['hostname', 'ip', 'ranges' => ['!192.168.0.0/24', 'any']], // any IP is allowed except IP in the specified subnet
            // ['hostname', 'ip', 'expandIPv6' => true], // expands IPv6 address to a full notation format
            
            [['loopback'], 'string', 'max' => 20],
            [['mac_address'], 'string', 'max' => 17],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sap_id' => 'SAP ID',
            'hostname' => 'Hostname',
            'loopback' => 'Loopback',
            'mac_address' => 'Mac Address',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {
            if (!Yii::$app->user->isGuest && $this->isNewRecord) {
                $this->created_by = $this->updated_by = Yii::$app->user->identity->id;
            } elseif (!Yii::$app->user->isGuest && !$this->isNewRecord) {
                $this->updated_by = Yii::$app->user->identity->id;
            }

            if ($this->isNewRecord) {
                $this->created_at = $this->updated_at = date('Y-m-d H:i:s');
            } else {
                $this->created_at = date('Y-m-d H:i:s', strtotime($this->created_at));
                $this->updated_at = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }
}
